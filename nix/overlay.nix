final: previous:
with final.haskell.lib;

let
  facturator =
    doBenchmark (
      buildStrictly (
        disableLibraryProfiling (
          final.haskellPackages.callCabal2nixWithOptions "facturator" (final.gitignoreSource (../.)) "--no-hpack" { }
        )
      )
    );
in
{
  facturator = justStaticExecutables final.haskellPackages.facturator;
  haskellPackages =
    previous.haskellPackages.override (
      old:
      {
        overrides =
          final.lib.composeExtensions
            (
              old.overrides or (_:
                _:
                { })
            )
            (
              self: super:
                {
                  "facturator" = facturator;
                }
            );
      }
    );
}
