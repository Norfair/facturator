module CsSyd.Facturator.Generate.Types where

import CsSyd.Facturator.Types
import Data.Text
import qualified Data.Time as Time
import Path

data GenerationAssignment = GenerationAssignment
  { genOutputDirective :: OutputDirective,
    genInvoiceFrom :: InvoiceFrom,
    genInvoiceTo :: InvoiceTo,
    genReference :: Maybe Text,
    genDate :: Time.Day,
    genDueDate :: Maybe Time.Day,
    genLines :: [InvoiceLine]
  }
  deriving (Show, Eq)

data OutputDirective
  = ToStdOut
  | ToFile (Path Abs File)
  deriving (Show, Eq)
