{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module CsSyd.Facturator.Generate where

import Control.Applicative
import Control.Monad
import CsSyd.Facturator.Generate.Types
import CsSyd.Facturator.OptParse.Types
import CsSyd.Facturator.Types
import Data.Aeson as JSON
import Data.Aeson.Encode.Pretty as JSON
import qualified Data.ByteString as SB
import qualified Data.ByteString.Char8 as SB8
import qualified Data.ByteString.Lazy as LB
import qualified Data.ByteString.Lazy.Char8 as LB8
import Data.List
import Data.Maybe
import qualified Data.Text as T
import qualified Data.Text.IO as T
import qualified Data.Time as Time
import qualified Data.Yaml as Yaml
import Path
import Path.IO
import System.Exit
import System.Process
import Text.Mustache

generate :: GenerationSettings -> IO ()
generate sets = do
  GenerationAssignment {..} <- assignGeneration sets
  let invoice =
        emptyInvoice
          { invoiceFrom = genInvoiceFrom,
            invoiceTo = genInvoiceTo,
            invoiceReference = genReference,
            invoiceDate = genDate,
            invoiceDueDate = genDueDate,
            invoiceLines = genLines
          }
  case genOutputDirective of
    ToStdOut -> SB8.putStrLn $ Yaml.encode invoice
    ToFile f -> writeJSON f invoice

assignGeneration :: GenerationSettings -> IO GenerationAssignment
assignGeneration GenerationSettings {..} =
  GenerationAssignment outputd <$> getfrom <*> getto
    <*> pure generateSetsReference
    <*> getdate
    <*> getduedate
    <*> getlines
  where
    outputd :: OutputDirective
    outputd = maybe ToStdOut ToFile generateSetsOutput
    getfrom :: IO InvoiceFrom
    getfrom =
      getJSONOrYAMLWith
        generateSetsFrom
        emptyInvoiceFrom
        ($(mkRelDir "from/") </>)
    getto :: IO InvoiceTo
    getto =
      getJSONOrYAMLWith generateSetsTo emptyInvoiceTo ($(mkRelDir "to/") </>)
    getJSONOrYAMLWith ::
      FromJSON a =>
      Maybe (Path Rel File) ->
      a ->
      (Path Rel File -> Path Rel File) ->
      IO a
    getJSONOrYAMLWith mf def_ func =
      case mf of
        Nothing -> pure def_
        Just tof -> do
          jpn <- replaceExtension ".json" $ func tof
          ypn <- replaceExtension ".yaml" $ func tof
          here <- getCurrentDir
          mjaf <- searchFile here jpn
          myaf <- searchFile here ypn
          case mjaf <|> myaf of
            Nothing ->
              die $
                unlines
                  [ "Files",
                    toFilePath jpn,
                    "and",
                    toFilePath ypn,
                    "not found anywhere under",
                    toFilePath here
                  ]
            Just af -> readJSONOrYaml af
    getdate :: IO Time.Day
    getdate =
      case generateSetsDay of
        Nothing -> do
          now <- Time.getZonedTime
          let nowutc = Time.zonedTimeToUTC now
          pure $ Time.utctDay nowutc
        Just d -> pure d
    getduedate =
      case generateSetsDueDays of
        Nothing -> pure Nothing
        Just i -> Just . Time.addDays i <$> getdate
    getlines =
      pure $
        generateSetsLines
          ++ case generateSetsPreset of
            EmptyLines -> []
            WaitingTime mt mp mps ->
              [ InvoiceLine
                  { lineDescription = "Waiting time",
                    lineQuantity = fromMaybe (read "NaN") mt,
                    linePrice =
                      Price
                        { priceQuantity = fromMaybe (read "NaN") mp,
                          priceSign = maybe "" T.pack mps
                        }
                  }
              ]

genPdfFor :: ToJSON (ForMustache a) => Template -> Path Abs File -> a -> IO ()
genPdfFor tmpl file val = do
  let mustacheVal = ForMustache val
  writeJSON file mustacheVal
  texFile <- replaceExtension ".tex" file
  let (errs, texContents) = checkedSubstitute tmpl $ toJSON mustacheVal
  unless (null errs) $
    die $
      unlines $
        [ "Substitution errors in template when substituting with data",
          LB8.unpack $ encodePretty mustacheVal
        ]
          ++ map show errs
  T.writeFile (toFilePath texFile) texContents
  let cmd = unwords ["latexmk", "-pdf", toFilePath $ filename texFile]
  (_, _, _, ph) <-
    createProcess $ (shell cmd) {cwd = Just $ toFilePath $ parent file}
  ec <- waitForProcess ph
  case ec of
    ExitSuccess -> pure ()
    ExitFailure c ->
      die $ unwords ["Command", cmd, "failed with exit code", show c]

writeJSON :: ToJSON a => Path Abs File -> a -> IO ()
writeJSON f a = do
  ensureDir $ parent f
  LB.writeFile (toFilePath f) (JSON.encodePretty a)

writeYaml :: ToJSON a => Path Abs File -> a -> IO ()
writeYaml f a = do
  ensureDir $ parent f
  SB.writeFile (toFilePath f) (Yaml.encode a)

readJSONOrYaml :: FromJSON a => Path Abs File -> IO a
readJSONOrYaml f = do
  mc <- forgivingAbsence $ LB.readFile $ toFilePath f
  case mc of
    Nothing -> die $ unwords ["File", toFilePath f, "not found."]
    Just contents ->
      case JSON.eitherDecode contents of
        Right res -> pure res
        Left err ->
          case Yaml.decodeEither' (LB.toStrict contents) of
            Left yerr ->
              die $
                unlines
                  [ "Failed to decode file as either json or yaml",
                    toFilePath f,
                    "json decoding error:",
                    show err,
                    "yaml decoding error:",
                    show yerr
                  ]
            Right res -> pure res

searchFile :: Path Abs Dir -> Path Rel File -> IO (Maybe (Path Abs File))
searchFile root file = do
  files <- snd <$> listDirRecur root
  pure $
    find
      ( \f ->
          isJust $
            stripPrefix (reverse $ toFilePath file) (reverse $ toFilePath f)
      )
      files
