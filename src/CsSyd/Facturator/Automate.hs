{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}
{-# LANGUAGE TemplateHaskell #-}

module CsSyd.Facturator.Automate where

import Control.Monad
import CsSyd.Facturator.Generate
import CsSyd.Facturator.Types
import Data.List
import Path
import Path.IO
import System.Exit
import Text.Mustache

automate :: IO ()
automate = do
  here <- getCurrentDir
  let rawDir = here </> rawDataDir
      autoDir = here </> autoGenDir
      manualDir = here </> manualDataDir
  fs <- snd <$> listDirRecur rawDir
  template <-
    do
      errOrTempl <-
        automaticCompile
          [toFilePath manualDir]
          (toFilePath $ manualDir </> mainTemplateFile)
      case errOrTempl of
        Left err ->
          die $ unwords ["Failed to compile invoice template", show err]
        Right tmpl -> pure tmpl
  forM_ (filter (not . hidden) fs) $ \f -> do
    i <- readJSONOrYaml f
    case stripProperPrefix rawDir f of
      Nothing -> pure ()
      Just rf -> genPdfFor template (autoDir </> rf) (i :: Invoice)

-- TODO filter all hidden files!
hidden :: Path Abs File -> Bool
hidden f =
  case fileExtension f of
    Nothing -> False
    Just e -> ".swp" `isSuffixOf` e

rawDataDir :: Path Rel Dir
rawDataDir = $(mkRelDir "raw")

autoGenDir :: Path Rel Dir
autoGenDir = $(mkRelDir "auto")

manualDataDir :: Path Rel Dir
manualDataDir = $(mkRelDir "manual")

mainTemplateFile :: Path Rel File
mainTemplateFile = $(mkRelFile "invoice.tex")
