{-# LANGUAGE DeriveGeneric #-}
{-# LANGUAGE FlexibleContexts #-}
{-# LANGUAGE FlexibleInstances #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE RecordWildCards #-}

module CsSyd.Facturator.Types
  ( module CsSyd.Facturator.Types,
    Price (..),
  )
where

import Data.Aeson
import Data.Text (Text)
import qualified Data.Text as T
import Data.Time as Time
import Data.Validity
import Data.Validity.Text ()
import Data.Validity.Time ()
import GHC.Generics (Generic)
import Text.Pandoc
import Text.Printf

newtype ForMustache a
  = ForMustache a
  deriving (Show, Eq, Generic)

instance ToJSON (ForMustache a) => ToJSON (ForMustache (Maybe a)) where
  toJSON (ForMustache Nothing) = Bool False
  toJSON (ForMustache (Just a)) = toJSON $ ForMustache a

instance ToJSON (ForMustache Text) where
  toJSON (ForMustache t) = toJSON t

instance ToJSON (ForMustache Day) where
  toJSON (ForMustache d) =
    object
      [ "strict" .= formatTime defaultTimeLocale "%Y-%m-%d" d,
        "full" .= formatTime defaultTimeLocale "%A %B %e %Y" d
      ]

newtype EmailAddress
  = EmailAddress Text
  deriving (Show, Eq, Generic)

instance Validity EmailAddress

instance FromJSON EmailAddress where
  parseJSON o = EmailAddress <$> parseJSON o

instance ToJSON EmailAddress where
  toJSON (EmailAddress t) = toJSON t

instance ToJSON (ForMustache EmailAddress) where
  toJSON (ForMustache (EmailAddress t)) = String t

emptyEmailAddress :: EmailAddress
emptyEmailAddress = EmailAddress ""

data PhoneNumber = PhoneNumber
  { phoneCountryCode :: Text,
    phoneNumber :: Text
  }
  deriving (Show, Eq, Generic)

instance Validity PhoneNumber

instance FromJSON PhoneNumber where
  parseJSON =
    withObject "PhoneNumber" $ \o ->
      PhoneNumber <$> o .: "country code" <*> o .: "number"

instance ToJSON PhoneNumber where
  toJSON PhoneNumber {..} =
    object ["country code" .= phoneCountryCode, "number" .= phoneNumber]

instance ToJSON (ForMustache PhoneNumber) where
  toJSON (ForMustache PhoneNumber {..}) =
    object
      [ "country-code" .= phoneCountryCode,
        "number" .= phoneNumber,
        "full-number" .= T.unwords [phoneCountryCode, phoneNumber]
      ]

emptyPhoneNumber :: PhoneNumber
emptyPhoneNumber = PhoneNumber {phoneCountryCode = "", phoneNumber = ""}

data Address = Address
  { addressName :: Text,
    addressCompany :: Text,
    addressStreet :: Text,
    addressNumber :: Text,
    addressPostCode :: Text,
    addressCity :: Text,
    addressCountry :: Text
  }
  deriving (Show, Eq, Generic)

instance Validity Address

instance FromJSON Address where
  parseJSON =
    withObject "Address" $ \o ->
      Address <$> o .: "name" <*> o .: "company" <*> o .: "street"
        <*> o .: "number"
        <*> o .: "post code"
        <*> o .: "city"
        <*> o .: "country"

instance ToJSON Address where
  toJSON Address {..} =
    object
      [ "name" .= addressName,
        "company" .= addressCompany,
        "street" .= addressStreet,
        "number" .= addressNumber,
        "post code" .= addressPostCode,
        "city" .= addressCity,
        "country" .= addressCountry
      ]

instance ToJSON (ForMustache Address) where
  toJSON (ForMustache Address {..}) =
    object
      [ "name" .= addressName,
        "company" .= addressCompany,
        "street" .= addressStreet,
        "number" .= addressNumber,
        "post-code" .= addressPostCode,
        "city" .= addressCity,
        "country" .= addressCountry
      ]

emptyAddress :: Address
emptyAddress =
  Address
    { addressName = "",
      addressCompany = "",
      addressStreet = "",
      addressNumber = "",
      addressPostCode = "",
      addressCity = "",
      addressCountry = ""
    }

newtype IBAN
  = IBAN Text
  deriving (Show, Eq, Generic)

instance Validity IBAN

instance FromJSON IBAN where
  parseJSON o = IBAN <$> parseJSON o

instance ToJSON IBAN where
  toJSON (IBAN t) = toJSON t

instance ToJSON (ForMustache IBAN) where
  toJSON (ForMustache (IBAN t)) =
    object ["strict" .= t, "by4" .= T.unwords (T.chunksOf 4 t)]

data Price = Price -- Use Hledger 'Amount'
  { priceQuantity :: Double,
    priceSign :: Text
  }
  deriving (Show, Eq, Generic)

instance Validity Price where
  validate p@Price {..} =
    mconcat
      [ genericValidate p,
        validateNotNaN priceQuantity,
        validateNotInfinite priceQuantity
      ]

instance FromJSON Price where
  parseJSON =
    withObject "Price" $ \o -> Price <$> o .: "quantity" <*> o .: "sign"

instance ToJSON Price where
  toJSON Price {..} = object ["quantity" .= priceQuantity, "sign" .= priceSign]

instance ToJSON (ForMustache Price) where
  toJSON (ForMustache Price {..}) =
    object
      ["full" .= T.unwords [T.pack $ printf "%.2f" priceQuantity, priceSign]]

priceMult :: Price -> Double -> Price
priceMult p d = p {priceQuantity = priceQuantity p * d}

-- this does not check whether the signs mtch
priceAdds :: [Price] -> Price
priceAdds [] = Price 0 ""
priceAdds ps = Price (sum $ map priceQuantity ps) $ priceSign $ head ps

data PaymentDetails = PaymentDetails
  { paymentBeneficiary :: Text,
    paymentIban :: Maybe IBAN,
    paymentBic :: Maybe Text,
    paymentExtra :: Maybe Text
  }
  deriving (Show, Eq, Generic)

instance Validity PaymentDetails

instance FromJSON PaymentDetails where
  parseJSON =
    withObject "PaymentDetails" $ \o ->
      PaymentDetails <$> o .: "beneficiary" <*> o .:? "iban" <*> o .:? "bic" <*> o .:? "extra"

instance ToJSON PaymentDetails where
  toJSON PaymentDetails {..} =
    object
      [ "beneficiary" .= paymentBeneficiary,
        "iban" .= paymentIban,
        "bic" .= paymentBic,
        "extra" .= paymentExtra
      ]

instance ToJSON (ForMustache PaymentDetails) where
  toJSON (ForMustache PaymentDetails {..}) =
    object
      [ "beneficiary" .= paymentBeneficiary,
        "iban" .= ForMustache paymentIban,
        "bic" .= paymentBic,
        "extra" .= paymentExtra
      ]

data Invoice = Invoice
  { invoiceFrom :: InvoiceFrom,
    invoiceTo :: InvoiceTo,
    invoiceDate :: Day,
    invoiceDueDate :: Maybe Day,
    invoiceReference :: Maybe Text,
    invoiceInvoiceNumber :: Int,
    invoiceLines :: [InvoiceLine]
  }
  deriving (Show, Eq, Generic)

instance Validity Invoice

instance FromJSON Invoice where
  parseJSON (Object o) =
    Invoice <$> o .: "from" <*> o .: "to" <*> o .: "date" <*> o .: "due date"
      <*> o .: "reference"
      <*> o .: "invoice-number"
      <*> o .: "lines"
  parseJSON _ = mempty

instance ToJSON Invoice where
  toJSON Invoice {..} =
    object
      [ "from" .= invoiceFrom,
        "to" .= invoiceTo,
        "date" .= invoiceDate,
        "due date" .= invoiceDueDate,
        "reference" .= invoiceReference,
        "invoice-number" .= invoiceInvoiceNumber,
        "lines" .= invoiceLines
      ]

instance ToJSON (ForMustache Invoice) where
  toJSON (ForMustache Invoice {..}) =
    object
      [ "from" .= ForMustache invoiceFrom,
        "to" .= ForMustache invoiceTo,
        "date" .= ForMustache invoiceDate,
        "due-date" .= ForMustache invoiceDueDate,
        "reference" .= ForMustache invoiceReference,
        "lines" .= map ForMustache invoiceLines,
        "invoice-number" .= invoiceInvoiceNumber,
        "total-amount"
          .= ForMustache (priceAdds $ map lineAmount invoiceLines)
      ]

emptyInvoice :: Invoice
emptyInvoice =
  Invoice
    { invoiceFrom = emptyInvoiceFrom,
      invoiceTo = emptyInvoiceTo,
      invoiceDate = Time.fromGregorian 1970 1 1,
      invoiceDueDate = Nothing,
      invoiceReference = Nothing,
      invoiceInvoiceNumber = 0,
      invoiceLines = []
    }

data InvoiceFrom = InvoiceFrom
  { fromAddress :: Address,
    fromPhone :: PhoneNumber,
    fromEmail :: EmailAddress,
    fromPaymentDetails :: PaymentDetails,
    fromUid :: Text,
    fromVatNr :: Maybe Text
  }
  deriving (Show, Eq, Generic)

instance Validity InvoiceFrom

instance FromJSON InvoiceFrom where
  parseJSON =
    withObject "InvoiceFrom" $ \o ->
      InvoiceFrom <$> o .: "address" <*> o .: "phone" <*> o .: "email"
        <*> o .: "payment details"
        <*> o .:? "uid" .!= ""
        <*> o .:? "vat-nr"

instance ToJSON InvoiceFrom where
  toJSON InvoiceFrom {..} =
    object
      [ "address" .= fromAddress,
        "phone" .= fromPhone,
        "email" .= fromEmail,
        "payment details" .= fromPaymentDetails,
        "uid" .= fromUid,
        "vat-nr" .= fromVatNr
      ]

instance ToJSON (ForMustache InvoiceFrom) where
  toJSON (ForMustache InvoiceFrom {..}) =
    object
      [ "address" .= ForMustache fromAddress,
        "phone" .= ForMustache fromPhone,
        "email" .= fromEmail,
        "payment-details" .= ForMustache fromPaymentDetails,
        "uid" .= fromUid,
        "vat-nr" .= fromVatNr
      ]

emptyInvoiceFrom :: InvoiceFrom
emptyInvoiceFrom =
  InvoiceFrom
    { fromAddress = emptyAddress,
      fromPhone = emptyPhoneNumber,
      fromEmail = EmailAddress "",
      fromPaymentDetails =
        PaymentDetails
          { paymentBeneficiary = "",
            paymentIban = Nothing,
            paymentBic = Nothing,
            paymentExtra = Nothing
          },
      fromUid = "",
      fromVatNr = Nothing
    }

data InvoiceTo = InvoiceTo
  { toAddress :: Address,
    toVATNumber :: Maybe Text
  }
  deriving (Show, Eq, Generic)

instance Validity InvoiceTo

instance FromJSON InvoiceTo where
  parseJSON (Object o) = InvoiceTo <$> o .: "address" <*> o .:? "vat-nr"
  parseJSON _ = mempty

instance ToJSON InvoiceTo where
  toJSON InvoiceTo {..} =
    object ["address" .= toAddress, "vat-nr" .= toVATNumber]

instance ToJSON (ForMustache InvoiceTo) where
  toJSON (ForMustache InvoiceTo {..}) =
    object
      [ "address" .= ForMustache toAddress,
        "vat-nr"
          .= case toVATNumber of
            Nothing -> ""
            Just vn -> "VAT: " <> vn
      ]

emptyInvoiceTo :: InvoiceTo
emptyInvoiceTo = InvoiceTo {toAddress = emptyAddress, toVATNumber = Nothing}

data InvoiceLine = InvoiceLine
  { lineDescription :: Text,
    lineQuantity :: Double,
    linePrice :: Price
  }
  deriving (Show, Eq, Generic)

instance Validity InvoiceLine where
  validate il@InvoiceLine {..} =
    mconcat
      [ genericValidate il,
        validateNotNaN lineQuantity,
        validateNotInfinite lineQuantity
      ]

instance FromJSON InvoiceLine where
  parseJSON (Object o) =
    InvoiceLine <$> o .: "description" <*> o .: "quantity" <*> o .: "unit price"
  parseJSON _ = mempty

instance ToJSON InvoiceLine where
  toJSON InvoiceLine {..} =
    object
      [ "description" .= lineDescription,
        "quantity" .= lineQuantity,
        "unit price" .= linePrice
      ]

instance ToJSON (ForMustache InvoiceLine) where
  toJSON (ForMustache l@InvoiceLine {..}) =
    object
      [ "description" .= markdownFor lineDescription,
        "quantity" .= lineQuantity,
        "unit-price" .= ForMustache linePrice,
        "amount" .= ForMustache (lineAmount l)
      ]

lineAmount :: InvoiceLine -> Price
lineAmount InvoiceLine {..} = priceMult linePrice lineQuantity

markdownFor :: Text -> Value
markdownFor t =
  object
    [ "raw" .= t,
      "rendered"
        .= either
          (T.pack . show)
          id
          ( runPure
              ((writeLaTeX def =<< (readMarkdown def t :: PandocPure Pandoc)) :: PandocPure Text)
          )
    ]
