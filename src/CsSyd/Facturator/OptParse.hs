{-# LANGUAGE RecordWildCards #-}

module CsSyd.Facturator.OptParse
  ( module CsSyd.Facturator.OptParse,
    module CsSyd.Facturator.OptParse.Types,
  )
where

import CsSyd.Facturator.OptParse.Types
import CsSyd.Facturator.Types
import Data.Maybe
import qualified Data.Text as T
import qualified Data.Time as Time
import Options.Applicative
import Path
import Path.IO
import System.Environment (getArgs)
import Text.Read

getInstructions :: IO Instructions
getInstructions = do
  (cmd, flags) <- getArguments
  config <- getConfiguration cmd flags
  combineToInstructions cmd flags config

combineToInstructions :: Command -> Flags -> Configuration -> IO Instructions
combineToInstructions cmd_ Flags Configuration = (,) <$> disp <*> pure Settings
  where
    disp =
      case cmd_ of
        CommandAutomate -> pure DispatchAutomate
        CommandGenerate fs -> DispatchGenerate <$> generationSettings fs

generationSettings :: GenerationFlags -> IO GenerationSettings
generationSettings GenerationFlags {..} =
  GenerationSettings
    <$> ( case generateOutput of
            Nothing -> pure Nothing
            Just fp -> Just <$> resolveFile' fp
        )
    <*> parseRel generateFrom
    <*> parseRel generateTo
    <*> pure (T.pack <$> generateReference)
    <*> ( case generateDay of
            Nothing -> pure Nothing
            Just d -> Just <$> Time.parseTimeM True Time.defaultTimeLocale "%F" d
        )
    <*> pure generateDueDays
    <*> pure generationPreset
    <*> mapM parseInvoiceLine generationLines
  where
    parseRel :: Maybe FilePath -> IO (Maybe (Path Rel File))
    parseRel Nothing = pure Nothing
    parseRel (Just f) = Just <$> parseRelFile f
    readPriceIO :: String -> String -> IO Price
    readPriceIO p s = Price <$> readIO p <*> pure (T.pack s)
    parseInvoiceLine :: GenerationLine -> IO InvoiceLine
    parseInvoiceLine GenerationLine {..} =
      InvoiceLine (T.pack generationLineDescription)
        <$> readIO generationLineQuantity
        <*> readPriceIO generationLinePrice generationLinePriceSign

getConfiguration :: Command -> Flags -> IO Configuration
getConfiguration _ _ = pure Configuration

getArguments :: IO Arguments
getArguments = do
  args <- getArgs
  let result = runArgumentsParser args
  handleParseResult result

runArgumentsParser :: [String] -> ParserResult Arguments
runArgumentsParser = execParserPure prefs_ argParser
  where
    prefs_ =
      defaultPrefs
        { prefShowHelpOnError = True,
          prefShowHelpOnEmpty = True
        }

argParser :: ParserInfo Arguments
argParser = info (helper <*> parseArgs) help_
  where
    help_ = fullDesc <> progDesc description
    description = "Facturator"

parseArgs :: Parser Arguments
parseArgs = (,) <$> parseCommand <*> parseFlags

parseCommand :: Parser Command
parseCommand =
  hsubparser $
    mconcat
      [ command "automate" parseCommandAutomate,
        command "generate" parseCommandGenerate
      ]

parseCommandAutomate :: ParserInfo Command
parseCommandAutomate = info parser modifier
  where
    parser = pure CommandAutomate
    modifier = fullDesc <> progDesc "Automate invoices"

parseCommandGenerate :: ParserInfo Command
parseCommandGenerate = info parser modifier
  where
    parser = CommandGenerate <$> parseGenerateFlags
    modifier = fullDesc <> progDesc "Generate a new invoice"
    parseGenerateFlags =
      GenerationFlags
        <$> option
          (Just <$> str)
          ( mconcat
              [ long "output",
                short 'o',
                metavar "FILE",
                help "Where to output the result to",
                showDefaultWith $ fromMaybe "Standard output",
                value Nothing
              ]
          )
        <*> option
          (Just <$> str)
          ( mconcat
              [ long "from",
                metavar "FILENAME",
                help "The 'from' to use, specified as a filename.",
                showDefaultWith $ fromMaybe "Empty InvoiceFrom",
                value Nothing
              ]
          )
        <*> option
          (Just <$> str)
          ( mconcat
              [ long "to",
                metavar "FILENAME",
                help "The 'to' to use, specified as a filename.",
                showDefaultWith $ fromMaybe "Empty InvoiceTo",
                value Nothing
              ]
          )
        <*> option
          (Just <$> str)
          ( mconcat
              [ long "reference",
                metavar "TEXT",
                help "The reference to use for the invoice",
                showDefaultWith $ maybe "Empty reference" show,
                value Nothing
              ]
          )
        <*> option
          (Just <$> str)
          ( mconcat
              [ long "date",
                metavar "DATE",
                help "The invoice date",
                showDefaultWith $ fromMaybe "Today",
                value Nothing
              ]
          )
        <*> option
          (Just <$> auto)
          ( mconcat
              [ long "due-days",
                metavar "NUMBER",
                help "The number of days between the invoice date and the due date",
                showDefaultWith $ maybe "Empty due date" show,
                value Nothing
              ]
          )
        <*> option
          ( maybeReader $ \s ->
              case words s of
                ["empty"] -> Just EmptyLines
                ["waiting"] -> Just $ WaitingTime Nothing Nothing Nothing
                ["waiting", hours] ->
                  WaitingTime <$> (Just <$> readMaybe hours) <*> pure Nothing
                    <*> pure Nothing
                ["waiting", hours, price] ->
                  WaitingTime <$> (Just <$> readMaybe hours)
                    <*> (Just <$> readMaybe price)
                    <*> pure Nothing
                ["waiting", hours, price, sign] ->
                  WaitingTime <$> (Just <$> readMaybe hours)
                    <*> (Just <$> readMaybe price)
                    <*> pure (Just sign)
                _ -> Nothing
          )
          ( mconcat
              [ long "preset",
                metavar "PRESET",
                help $
                  unlines
                    [ "The preset to determine where to get the lines of the invoice",
                      "options: [empty, waiting [Hours] [Price] [PriceSign]]"
                    ],
                showDefault,
                value EmptyLines
              ]
          )
        <*> many parseGeneratorLine

parseGeneratorLine :: Parser GenerationLine
parseGeneratorLine =
  GenerationLine
    <$> strOption
      ( mconcat
          [ long "description",
            metavar "DESCRIPTION",
            help "The description of the line"
          ]
      )
    <*> strOption
      ( mconcat
          [long "quantity", metavar "QUANTITY", help "The quantity of the line"]
      )
    <*> strOption
      (mconcat [long "price", metavar "PRICE", help "The price of the line"])
    <*> strOption
      ( mconcat
          [long "sign", metavar "CURRENCY", help "The currency sign of the line"]
      )

parseFlags :: Parser Flags
parseFlags = pure Flags
