module CsSyd.Facturator.OptParse.Types where

import CsSyd.Facturator.Types
import Data.Text as T
import qualified Data.Time as Time
import Path

type Arguments = (Command, Flags)

type Instructions = (Dispatch, Settings)

data Command
  = CommandAutomate
  | CommandGenerate GenerationFlags
  deriving (Show, Eq)

data Flags
  = Flags
  deriving (Show, Eq)

data Configuration
  = Configuration
  deriving (Show, Eq)

data Dispatch
  = DispatchAutomate
  | DispatchGenerate GenerationSettings
  deriving (Show, Eq)

data Settings
  = Settings
  deriving (Show, Eq)

data GenerationFlags = GenerationFlags
  { generateOutput :: Maybe FilePath,
    generateFrom :: Maybe FilePath,
    generateTo :: Maybe FilePath,
    generateReference :: Maybe String,
    generateDay :: Maybe String,
    generateDueDays :: Maybe Integer,
    generationPreset :: GenerationPreset,
    generationLines :: [GenerationLine]
  }
  deriving (Show, Eq)

data GenerationLine = GenerationLine
  { generationLineDescription :: String,
    generationLineQuantity :: String,
    generationLinePrice :: String,
    generationLinePriceSign :: String
  }
  deriving (Show, Eq)

data GenerationPreset
  = EmptyLines
  | WaitingTime (Maybe Double) (Maybe Double) (Maybe String)
  deriving (Show, Eq)

data GenerationSettings = GenerationSettings
  { generateSetsOutput :: Maybe (Path Abs File),
    generateSetsFrom :: Maybe (Path Rel File),
    generateSetsTo :: Maybe (Path Rel File),
    generateSetsReference :: Maybe Text,
    generateSetsDay :: Maybe Time.Day,
    generateSetsDueDays :: Maybe Integer,
    generateSetsPreset :: GenerationPreset,
    generateSetsLines :: [InvoiceLine]
  }
  deriving (Show, Eq)
