module CsSyd.Facturator where

import CsSyd.Facturator.Automate
import CsSyd.Facturator.Generate
import CsSyd.Facturator.OptParse

facturator :: IO ()
facturator = do
  (disp, Settings) <- getInstructions
  dispatch disp

dispatch :: Dispatch -> IO ()
dispatch DispatchAutomate = automate
dispatch (DispatchGenerate ga) = generate ga
